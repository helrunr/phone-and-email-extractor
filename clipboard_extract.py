import pyperclip, re

'''
area code (optional)
separator
first 3 digits
separator
last 4 digits
extension (optional)
'''
ph_regex = re.compile(r'''(
    (\d{3}|\(\d{3}\))?
    (\s|-|\.)?
    (\d{3})
    (\s|-|\.)
    (\d{4})
    (\s*(ext|x|ext.)\s*(\d{2,5}))?
    )''', re.VERBOSE)

'''
username
@ symbol
domain name
extension
'''
email_regex = re.compile(r'''(
    [a-zA-Z0-9._%+-]+
    @
    [a-zA-Z0-9.-]+
    (\.[a-zA-Z]{2,4})
    )''', re.VERBOSE)

text = str(pyperclip.paste())
matches = []

# There is a tuple for each match, and each tuple contains strings
# for each group in the regular expression. 
for groups in ph_regex.findall(text):
    # Normalize phone numbers
    phone_num = '-'.join([groups[1],groups[3],groups[5]])
    if groups[8] != '':
        phone_num += ' x' + groups[8]
    matches.append(phone_num)

# Group 0 stores email addresses
for groups in email_regex.findall(text):
    matches.append(groups[0])

if len(matches) > 0:
    pyperclip.copy('\n'.join(matches))
    print('Copied.')
    print('\n'.join(matches))
else:
    print('No matches found.')