# Clipboard Extractor
A utility that will print particular patterns you have copied in the clipboard to command line.
Configured by default to pull out email addresses and phone numbers.

Copy a body of text that you wish analyze and run the program to see the extracted copy.

